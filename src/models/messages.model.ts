import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['messages', './dist/jsons/messages.json'],
    ['message-details', './dist/jsons/message-details.json']
]);

export class MessagesModel implements Model {

    getAll(): any {
        return fs.readFileSync(JSONS_PATHS.get('messages'));
    }

    getById(): any {
        return fs.readFileSync(JSONS_PATHS.get('message-details'));
    }
}
