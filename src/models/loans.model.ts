import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['loans', './jsons/loans.json'],
    ['loan-details', './jsons/loan-details.json'],
]);

export class LoansModel implements Model {

    getAll(): any {
        return fs.readFileSync(JSONS_PATHS.get('loans'));
    }

    getById(): any {
        return fs.readFileSync(JSONS_PATHS.get('loan-details'));
    }
}
