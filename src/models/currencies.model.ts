import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['currencies', './dist/jsons/currencies.json']
]);

export class CurrenciesModel implements Model {

    getAll(): any {
        return fs.readFileSync(JSONS_PATHS.get('currencies'));
    }

    getById(): any {
        return null;
    }
}
