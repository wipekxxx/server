import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['domestic-beneficiaries', './dist/jsons/domestic-beneficiaries.json'],
    ['foreign-beneficiaries', './dist/jsons/foreign-beneficiaries.json'],
    ['zus-beneficiaries', './dist/jsons/zus-beneficiaries.json']
]);

export class BeneficiariesModel implements Model {

    getAll(): any {
        return fs.readFileSync(JSONS_PATHS.get('domestic-beneficiaries'));
    }

    getById(): any {
        return fs.readFileSync(JSONS_PATHS.get('domestic-beneficiaries'));
    }

    getAllDomesticBeneficiaries() {
        return fs.readFileSync(JSONS_PATHS.get('domestic-beneficiaries'));
    }

    getAllForeignBeneficiaries() {
        return fs.readFileSync(JSONS_PATHS.get('foreign-beneficiaries'));
    }

    getAllZusBeneficiaries() {
        return fs.readFileSync(JSONS_PATHS.get('zus-beneficiaries'));
    }
}
