import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['deposits', './dist/jsons/deposits.json'],
    ['deposit-details', './dist/jsons/deposit-details.json'],
    ['deposit-deactivate', './dist/jsons/deposit-deactivate.json'],
    ['deposit-deactivate-accounts', './dist/jsons/deposit-deactivate-accounts.json']
]);

export class DepositsModel implements Model {

    getAll(): any {
        return fs.readFileSync(JSONS_PATHS.get('deposits'));
    }

    getById(): any {
        return fs.readFileSync(JSONS_PATHS.get('deposit-details'));
    }

    getDepositDeactivateDetails() {
        return fs.readFileSync(JSONS_PATHS.get('deposit-deactivate'));
    }

    getDepositDeactivateAccounts() {
        return fs.readFileSync(JSONS_PATHS.get('deposit-deactivate-accounts'));
    }
}
