import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['currencies-pl', './dist/jsons/dictionaries-currencies-pl.json'],
    ['card-filter-statuses-pl', './dist/jsons/dictionaries-card-filter-statuses-pl.json'],
    ['card-filter-types-pl', './dist/jsons/dictionaries-card-filter-types-pl.json'],
    ['card-history-filter-statuses-pl', './dist/jsons/dictionaries-card-history-filter-statuses-pl.json'],
    ['card-date-periods-pl', './dist/jsons/dictionaries-card-date-periods-pl.json'],
    ['deposit-statuses-pl', './dist/jsons/dictionaries-deposit-statuses-pl.json'],
    ['card-objection-reasons-pl', './dist/jsons/dictionaries-card-objection-reasons-pl.json'],
    ['loan-statuses-pl', './dist/jsons/dictionaries-loan-statuses-pl.json'],
    ['message-filter-statuses-pl', './dist/jsons/dictionaries-message-filter-statuses-pl.json'],
    ['message-filter-date-periods-pl', './dist/jsons/dictionaries-message-filter-date-periods-pl.json'],
    ['message-filter-types-pl', './dist/jsons/dictionaries-message-filter-types-pl.json'],
    ['status-success-pl', './dist/jsons/dictionaries-status-success-pl.json'],
    ['domestic-transfer-types-pl', './dist/jsons/dictionaries-domestic-transfer-types-pl.json'],
    ['countries-pl', './dist/jsons/dictionaries-countries-pl.json'],
    ['currency-dates-pl', './dist/jsons/dictionaries-currency-dates-pl.json'],
    ['costs-and-commissions-pl', './dist/jsons/dictionaries-costs-and-commissions-pl.json'],
    ['billing-systems-pl', './dist/jsons/dictionaries-billing-systems-pl.json'],
    ['payment-types-pl', './dist/jsons/dictionaries-payment-types-pl.json'],
    ['identificatories-pl', './dist/jsons/dictionaries-identificatories-pl.json'],
    ['months-pl', './dist/jsons/dictionaries-months-pl.json'],
    ['declaration-numbers-pl', './dist/jsons/dictionaries-declaration-numbers-pl.json']
]);

export class DictionariesModel {

    getCurrencies(): any {
        return fs.readFileSync(JSONS_PATHS.get('currencies-pl'));
    }

    getCardFilterStatuses(): any {
        return fs.readFileSync(JSONS_PATHS.get('card-filter-statuses-pl'));
    }

    getCardFilterTypes(): any {
        return fs.readFileSync(JSONS_PATHS.get('card-filter-types-pl'));
    }

    getCardHistoryFilterStatuses(): any {
        return fs.readFileSync(JSONS_PATHS.get('card-history-filter-statuses-pl'));
    }

    getCardFilterDatePeriods(): any {
        return fs.readFileSync(JSONS_PATHS.get('card-date-periods-pl'));
    }

    getDepositStatuses(): any {
        return fs.readFileSync(JSONS_PATHS.get('deposit-statuses-pl'));
    }

    getCardObjectionReasons() {
        return fs.readFileSync(JSONS_PATHS.get('card-objection-reasons-pl'));
    }

    getLoanStatuses() {
        return fs.readFileSync(JSONS_PATHS.get('loan-statuses-pl'));
    }

    getMessageStatuses() {
        return fs.readFileSync(JSONS_PATHS.get('message-filter-statuses-pl'));
    }

    getMessageFilterDatePeriods() {
        return fs.readFileSync(JSONS_PATHS.get('message-filter-date-periods-pl'));
    }

    getMessageFilterTypes() {
        return fs.readFileSync(JSONS_PATHS.get('message-filter-types-pl'));
    }

    getStatusSuccess() {
        return fs.readFileSync(JSONS_PATHS.get('status-success-pl'));
    }

    getDomesticTransferTypes() {
        return fs.readFileSync(JSONS_PATHS.get('domestic-transfer-types-pl'));
    }

    getCountries() {
        return fs.readFileSync(JSONS_PATHS.get('countries-pl'));
    }

    getCurrencyDates() {
        return fs.readFileSync(JSONS_PATHS.get('currency-dates-pl'));
    }

    getCostsAndCommissions() {
        return fs.readFileSync(JSONS_PATHS.get('costs-and-commissions-pl'));
    }

    getBillingSystems() {
        return fs.readFileSync(JSONS_PATHS.get('billing-systems-pl'));
    }

    getPaymentTypes() {
        return fs.readFileSync(JSONS_PATHS.get('payment-types-pl'));
    }

    getIdentificatories() {
        return fs.readFileSync(JSONS_PATHS.get('identificatories-pl'));
    }

    getMonths() {
        return fs.readFileSync(JSONS_PATHS.get('months-pl'));
    }

    getDeclarationNumbers() {
        return fs.readFileSync(JSONS_PATHS.get('declaration-numbers-pl'));
    }
}
