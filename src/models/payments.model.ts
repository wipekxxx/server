import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['accounts', './dist/jsons/payments-accounts.json'],
    ['contributions', './dist/jsons/contributions.json']
]);

export class PaymentsModel implements Model {

    getAccounts(): any {
        return fs.readFileSync(JSONS_PATHS.get('accounts'));
    }

    getAll(): any {
        return undefined;
    }

    getById(): any {
        return undefined;
    }

    getContributions() {
        return fs.readFileSync(JSONS_PATHS.get('contributions'));
    }
}
