import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['accounts', './dist/jsons/accounts.json'],
    ['account-details', './dist/jsons/account-details.json']
]);

export class AccountsModel implements Model {

    getAll(): any {
        return fs.readFileSync(JSONS_PATHS.get('accounts'));
    }

    getById(): any {
        return fs.readFileSync(JSONS_PATHS.get('account-details'));
    }
}
