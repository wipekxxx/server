import { Model } from './model';
import * as fs from 'fs';

const JSONS_PATHS = new Map([
    ['cards', './dist/jsons/cards.json'],
    ['card-details', './dist/jsons/card-details.json'],
    ['cards-accounts', './dist/jsons/cards-accounts.json'],
    ['card-operations', './dist/jsons/card-operations.json'],
    ['card-deactivate', './dist/jsons/card-deactivate.json']
]);

export class CardsModel implements Model {

    getAll(): any {
        return fs.readFileSync(JSONS_PATHS.get('cards'));
    }

    getById(): any {
        return fs.readFileSync(JSONS_PATHS.get('card-details'));
    }

    getAllAccountsAssignedToCards(): any {
        return fs.readFileSync(JSONS_PATHS.get('cards-accounts'));
    }

    getCardOperations(): any {
        return fs.readFileSync(JSONS_PATHS.get('card-operations'));
    }

    getCardDeactivateDetails() {
        return fs.readFileSync(JSONS_PATHS.get('card-deactivate'));
    }
}
