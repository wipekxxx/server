import { Controller } from './controller';
import { Request, Response } from 'express';
import { CardsModel } from '../models/cards.model';
import { DictionariesModel } from '../models/dictionaries.model';

export class CardsController implements Controller {
    private _cardsModel: CardsModel;
    private _dictionariesModel: DictionariesModel;

    constructor() {
        this._cardsModel = new CardsModel();
        this._dictionariesModel = new DictionariesModel();
    }

    getAll(req: Request, res: Response) {
        res.send(this._cardsModel.getAll());
    }

    getById(req: Request, res: Response) {
        res.send(this._cardsModel.getById());
    }

    getAllAccountsAssignedToCards(req: Request, res: Response) {
        res.send(this._cardsModel.getAllAccountsAssignedToCards());
    }

    getCardOperations(req: Request, res: Response) {
        res.send(this._cardsModel.getCardOperations());
    }

    getCardDeactivateDetails(req: Request, res: Response) {
        res.send(this._cardsModel.getCardDeactivateDetails());
    }

    deactivateCard(req: Request, res: Response) {
        res.send(this._cardsModel.getCardDeactivateDetails());
    }
}
