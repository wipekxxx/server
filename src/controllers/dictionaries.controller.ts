import { DictionariesModel } from '../models/dictionaries.model';
import { Request, Response } from 'express';


export class DictionariesController {
    private _model: DictionariesModel;

    constructor() {
        this._model = new DictionariesModel();
    }

    getCurrencies(req: Request, res: Response) {
        res.send(this._model.getCurrencies());
    }

    getCardFilterStatuses(req: Request, res: Response) {
        res.send(this._model.getCardFilterStatuses());
    }

    getCardFilterTypes(req: Request, res: Response) {
        res.send(this._model.getCardFilterTypes());
    }

    getCardHistoryFilterStatuses(req: Request, res: Response) {
        res.send(this._model.getCardHistoryFilterStatuses());
    }

    getCardFilterDatePeriods(req: Request, res: Response) {
        res.send(this._model.getCardFilterDatePeriods());
    }

    getDepositStatuses(req: Request, res: Response) {
        res.send(this._model.getDepositStatuses());
    }

    getCardObjectionReasons(req: Request, res: Response) {
        res.send(this._model.getCardObjectionReasons());
    }

    getLoanStatuses(req: Request, res: Response) {
        res.send(this._model.getLoanStatuses());
    }

    getMessageStatuses(req: Request, res: Response) {
        res.send(this._model.getMessageStatuses());
    }

    getMessageFilterDatePeriods(req: Request, res: Response) {
        res.send(this._model.getMessageFilterDatePeriods());
    }

    getMessageFilterTypes(req: Request, res: Response) {
        res.send(this._model.getMessageFilterTypes());
    }

    getDomesticTransferTypes(req: Request, res: Response) {
        res.send(this._model.getDomesticTransferTypes());
    }

    getCountries(req: Request, res: Response) {
        res.send(this._model.getCountries());
    }

    getCurrencyDates(req: Request, res: Response) {
        res.send(this._model.getCurrencyDates());
    }

    getCostsAndCommissions(req: Request, res: Response) {
        res.send(this._model.getCostsAndCommissions());
    }

    getBillingSystems(req: Request, res: Response) {
        res.send(this._model.getBillingSystems());
    }

    getPaymentTypes(req: Request, res: Response) {
        res.send(this._model.getPaymentTypes());
    }

    getIdentificatories(req: Request, res: Response) {
        res.send(this._model.getIdentificatories());
    }

    getMonths(req: Request, res: Response) {
        res.send(this._model.getMonths());
    }

    getDeclarationNumbers(req: Request, res: Response) {
        res.send(this._model.getDeclarationNumbers());
    }
}
