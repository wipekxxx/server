import { Controller } from './controller';
import { Request, Response } from 'express';
import { PaymentsModel } from '../models/payments.model';
import { DictionariesModel } from '../models/dictionaries.model';

export class PaymentsController implements Controller {
    private _model: PaymentsModel;
    private _dictionariesModel: DictionariesModel;

    constructor() {
        this._model = new PaymentsModel();
        this._dictionariesModel = new DictionariesModel();
    }

    getAccounts(req: Request, res: Response) {
        res.send(this._model.getAccounts());
    }

    getAll(req: Request, res: Response): any {
        return undefined;
    }

    getById(req: Request, res: Response): any {
        return undefined;
    }

    signAndSendTransfer(req: Request, res: Response) {
        res.send(this._dictionariesModel.getStatusSuccess());
    }

    saveTransfer(req: Request, res: Response) {
        res.send(this._dictionariesModel.getStatusSuccess());
    }

    signTransfer(req: Request, res: Response) {
        res.send(this._dictionariesModel.getStatusSuccess());
    }

    getContributions(req: Request, res: Response) {
        res.send(this._model.getContributions());
    }
}
