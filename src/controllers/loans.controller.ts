import { Controller } from './controller';
import { Request, Response } from 'express';
import { LoansModel } from '../models/loans.model';

export class LoansController implements Controller {
    private _model: LoansModel;

    constructor() {
        this._model = new LoansModel();
    }

    getAll(req: Request, res: Response) {
        res.send(this._model.getAll());
    }

    getById(req: Request, res: Response) {
        res.send(this._model.getById());
    }

}
