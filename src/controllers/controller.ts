import { Request, Response } from 'express';

export interface Controller {
    getAll(req: Request, res: Response): any;
    getById(req: Request, res: Response): any;
}
