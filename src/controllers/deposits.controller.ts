import { Controller } from './controller';
import { Request, Response } from 'express';
import { DepositsModel } from '../models/deposits.model';
import { DictionariesModel } from '../models/dictionaries.model';

export class DepositsController implements Controller {
    private _depositsModel: DepositsModel;
    private _dictionariesModel: DictionariesModel;

    constructor() {
        this._depositsModel = new DepositsModel();
        this._dictionariesModel = new DictionariesModel();
    }

    getAll(req: Request, res: Response): any {
        res.send(this._depositsModel.getAll());
    }

    getById(req: Request, res: Response): any {
        res.send(this._depositsModel.getById());
    }

    getDepositsDeactivateDetails(req: Request, res: Response) {
        res.send(this._depositsModel.getDepositDeactivateDetails());
    }

    getDepositDeactivateAccounts(req: Request, res: Response) {
        res.send(this._depositsModel.getDepositDeactivateAccounts());
    }

    deactivateDeposit(req: Request, res: Response) {
        res.send(this._dictionariesModel.getStatusSuccess())
    }
}
