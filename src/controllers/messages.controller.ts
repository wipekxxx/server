import { MessagesModel } from '../models/messages.model';
import { Controller } from './controller';
import { Request, Response } from 'express';

export class MessagesController implements Controller {
    private _model: MessagesModel;

    constructor() {
        this._model = new MessagesModel();
    }

    getAll(req: Request, res: Response) {
        res.send(this._model.getAll());
    }

    getById(req: Request, res: Response) {
        res.send(this._model.getById());
    }
}
