import { CurrenciesModel } from '../models/currencies.model';
import { Controller } from './controller';
import { Request, Response } from 'express';

export class CurrenciesController implements Controller {
    private _model: CurrenciesModel;

    constructor() {
        this._model = new CurrenciesModel();
    }

    getAll(req: Request, res: Response) {
        res.send(this._model.getAll());
    }

    getById(req: Request, res: Response) {
        res.send(this._model.getById());
    }
}
