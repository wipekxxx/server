import { Controller } from './controller';
import { Request, Response } from 'express';
import { BeneficiariesModel } from '../models/beneficiaries.model';

export class BeneficiariesController implements Controller {
    private _model: BeneficiariesModel;

    constructor() {
        this._model = new BeneficiariesModel();
    }

    getAll(req: Request, res: Response) {
        res.send(this._model.getAll());
    }

    getById(req: Request, res: Response) {
        res.send(this._model.getById());
    }

    getAllDomesticBeneficiaries(req: Request, res: Response) {
        res.send(this._model.getAllDomesticBeneficiaries());
    }

    getAllForeignBeneficiaries(req: Request, res: Response) {
        res.send(this._model.getAllForeignBeneficiaries());
    }

    getAllZusBeneficiaries(req: Request, res: Response) {
        res.send(this._model.getAllZusBeneficiaries());
    }
}
