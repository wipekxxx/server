import { AccountsModel } from '../models/accounts.model';
import { Controller } from './controller';
import { Request, Response } from 'express';

export class AccountsController implements Controller {
    private _model: AccountsModel;

    constructor() {
        this._model = new AccountsModel();
    }

    getAll(req: Request, res: Response) {
        res.send(this._model.getAll());
    }

    getById(req: Request, res: Response) {
        res.send(this._model.getById());
    }
}
