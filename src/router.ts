import { AccountsController } from './controllers/accounts.controller';
import { CardsController } from './controllers/cards.controller';
import { DictionariesController } from './controllers/dictionaries.controller';
import { Request, Response } from 'express';
import { DepositsController } from './controllers/deposits.controller';
import { LoansController } from './controllers/loans.controller';
import { MessagesController } from './controllers/messages.controller';
import { PaymentsController } from './controllers/payments.controller';
import { BeneficiariesController } from './controllers/beneficiaries.controller';
import { CurrenciesController } from './controllers/currencies.controller';

export class Router {
    private _app: any;
    private _accountsController: AccountsController = new AccountsController();
    private _dictionariesController: DictionariesController = new DictionariesController();
    private _cardsController: CardsController = new CardsController();
    private _depositsController: DepositsController = new DepositsController();
    private _loanController: LoansController = new LoansController();
    private _messagesController: MessagesController = new MessagesController();
    private _paymentsController: PaymentsController = new PaymentsController();
    private _beneficiariesController: BeneficiariesController = new BeneficiariesController();
    private _currenciesController: CurrenciesController = new CurrenciesController();

    constructor(app: any) {
        this._app = app;
    }

    public setRoutes() {
        this.setAccountRoutes();
        this.setCardsRoutes();
        this.setDictionariesRoutes();
        this.setDepositsRoutes();
        this.setLoansRoutes();
        this.setMessagesRoutes();
        this.setPaymentsRoutes();
        this.setBeneficiariesRoutes();
        this.setCurrenciesRoutes();
    }

    private setAccountRoutes() {
        this._app.get('/accounts',
            (req: Request, res: Response) => this._accountsController.getAll(req, res));

        this._app.get('/accounts/8d342f9b-aa4b-4d06-9f7f-26126b107b07',
            (req: Request, res: Response) => this._accountsController.getById(req, res));
    }

    private setCardsRoutes() {
        this._app.get('/cards',
            (req: Request, res: Response) => this._cardsController.getAll(req, res));

        this._app.get('/cards/UUID1',
            (req: Request, res: Response) => this._cardsController.getById(req, res));

        this._app.get('/cards/accounts',
            (req: Request, res: Response) => this._cardsController.getAllAccountsAssignedToCards(req, res));

        this._app.get('/cards/UUID1/operations',
            (req: Request, res: Response) => this._cardsController.getCardOperations(req, res));

        this._app.get('/cards/UUID1/deactivate-details',
            (req: Request, res: Response) => this._cardsController.getCardDeactivateDetails(req, res));

        this._app.get('/cards/UUID1/deactivate',
            (req: Request, res: Response) => this._cardsController.deactivateCard(req, res));
    }

    private setDictionariesRoutes() {
        this._app.get('/dictionaries/currencies/pl',
            (req: Request, res: Response) => this._dictionariesController.getCurrencies(req, res));

        this._app.get('/dictionaries/card-filter-statuses/pl',
            (req: Request, res: Response) => this._dictionariesController.getCardFilterStatuses(req, res));

        this._app.get('/dictionaries/card-filter-types/pl',
            (req: Request, res: Response) => this._dictionariesController.getCardFilterTypes(req, res));

        this._app.get('/dictionaries/card-history-filter-statuses/pl',
            (req: Request, res: Response) => this._dictionariesController.getCardHistoryFilterStatuses(req, res));

        this._app.get('/dictionaries/card-filter-data-periods/pl',
            (req: Request, res: Response) => this._dictionariesController.getCardFilterDatePeriods(req, res));

        this._app.get('/dictionaries/cards-objection-reasons/pl',
            (req: Request, res: Response) => this._dictionariesController.getCardObjectionReasons(req, res));

        this._app.get('/dictionaries/deposit-statuses/pl',
            (req: Request, res: Response) => this._dictionariesController.getDepositStatuses(req, res));

        this._app.get('/dictionaries/message-statuses/pl',
            (req: Request, res: Response) => this._dictionariesController.getMessageStatuses(req, res));

        this._app.get('/dictionaries/loan-statuses/pl',
            (req: Request, res: Response) => this._dictionariesController.getLoanStatuses(req, res));

        this._app.get('/dictionaries/message-filter-date-periods/pl',
            (req: Request, res: Response) => this._dictionariesController.getMessageFilterDatePeriods(req, res));

        this._app.get('/dictionaries/message-filter-types/pl',
            (req: Request, res: Response) => this._dictionariesController.getMessageFilterTypes(req, res));

        this._app.get('/dictionaries/domestic-transfer-types/pl',
            (req: Request, res: Response) => this._dictionariesController.getDomesticTransferTypes(req, res));

        this._app.get('/dictionaries/countries/pl',
            (req: Request, res: Response) => this._dictionariesController.getCountries(req, res));

        this._app.get('/dictionaries/currency-dates/pl',
            (req: Request, res: Response) => this._dictionariesController.getCurrencyDates(req, res));

        this._app.get('/dictionaries/costs-and-commissions/pl',
            (req: Request, res: Response) => this._dictionariesController.getCostsAndCommissions(req, res));

        this._app.get('/dictionaries/billing-systems/pl',
            (req: Request, res: Response) => this._dictionariesController.getBillingSystems(req, res));

        this._app.get('/dictionaries/payment-types/pl',
            (req: Request, res: Response) => this._dictionariesController.getPaymentTypes(req, res));

        this._app.get('/dictionaries/identificatories/pl',
            (req: Request, res: Response) => this._dictionariesController.getIdentificatories(req, res));

        this._app.get('/dictionaries/months/pl',
            (req: Request, res: Response) => this._dictionariesController.getMonths(req, res));

        this._app.get('/dictionaries/declaration-numbers/pl',
            (req: Request, res: Response) => this._dictionariesController.getDeclarationNumbers(req, res));
    }

    private setDepositsRoutes() {
        this._app.get('/deposits',
            (req: Request, res: Response) => this._depositsController.getAll(req, res));

        this._app.get('/deposits/657543423',
            (req: Request, res: Response) => this._depositsController.getById(req, res));

        this._app.get('/deposits/657543423/deactivate-details',
            (req: Request, res: Response) => this._depositsController.getDepositsDeactivateDetails(req, res));

        this._app.post('/deposits/657543423/deactivate',
            (req: Request, res: Response) => this._depositsController.deactivateDeposit(req, res));

        this._app.get('/deposits/657543423/deactivate/accounts',
            (req: Request, res: Response) => this._depositsController.getDepositDeactivateAccounts(req, res));
    }

    private setLoansRoutes() {
        this._app.get('/loans',
            (req: Request, res: Response) => this._loanController.getAll(req, res));

        this._app.get('/loans/123456789',
            (req: Request, res: Response) => this._loanController.getById(req, res));
    }

    private setMessagesRoutes() {
        this._app.get('/messages',
            (req: Request, res: Response) => this._messagesController.getAll(req, res));

        this._app.get('/messages/12345',
            (req: Request, res: Response) => this._messagesController.getById(req, res));
    }

    private setPaymentsRoutes() {
        this._app.get('/payments/accounts',
            (req: Request, res: Response) => this._paymentsController.getAccounts(req, res));

        this.setDomesticTransferRoutes();
        this.setInternalTransferRoutes();
        this.setForeignTransferRoutes();
        this.setZusTransferRoutes();
    }

    private setDomesticTransferRoutes() {
        this._app.post('/payments/domestic-transfer/signAndSend',
            (req: Request, res: Response) => this._paymentsController.signAndSendTransfer(req, res));

        this._app.post('/payments/domestic-transfer/save',
            (req: Request, res: Response) => this._paymentsController.saveTransfer(req, res));

        this._app.post('/payments/domestic-transfer/sign',
            (req: Request, res: Response) => this._paymentsController.signTransfer(req, res));
    }

    private setInternalTransferRoutes() {
        this._app.post('/payments/internal-transfer/signAndSend',
            (req: Request, res: Response) => this._paymentsController.signAndSendTransfer(req, res));

        this._app.post('/payments/internal-transfer/save',
            (req: Request, res: Response) => this._paymentsController.saveTransfer(req, res));

        this._app.post('/payments/internal-transfer/sign',
            (req: Request, res: Response) => this._paymentsController.signTransfer(req, res));
    }

    private setForeignTransferRoutes() {
        this._app.post('/payments/foreign-transfer/signAndSend',
            (req: Request, res: Response) => this._paymentsController.signAndSendTransfer(req, res));

        this._app.post('/payments/foreign-transfer/save',
            (req: Request, res: Response) => this._paymentsController.saveTransfer(req, res));

        this._app.post('/payments/foreign-transfer/sign',
            (req: Request, res: Response) => this._paymentsController.signTransfer(req, res));
    }

    private setBeneficiariesRoutes() {
        this._app.get('/payments/beneficiaries/domestic-transfer',
            (req: Request, res: Response) => this._beneficiariesController.getAllDomesticBeneficiaries(req, res));

        this._app.get('/payments/beneficiaries/foreign-transfer',
            (req: Request, res: Response) => this._beneficiariesController.getAllForeignBeneficiaries(req, res));

        this._app.get('/payments/beneficiaries/zus-transfer',
            (req: Request, res: Response) => this._beneficiariesController.getAllZusBeneficiaries(req, res));
    }

    private setCurrenciesRoutes() {
        this._app.get('/currencies',
            (req: Request, res: Response) => this._currenciesController.getAll(req, res));
    }

    private setZusTransferRoutes() {
        this._app.get('/payments/contributions',
            (req: Request, res: Response) => this._paymentsController.getContributions(req, res));

        this._app.post('/payments/zus-transfer/signAndSend',
            (req: Request, res: Response) => this._paymentsController.signAndSendTransfer(req, res));

        this._app.post('/payments/zus-transfer/save',
            (req: Request, res: Response) => this._paymentsController.saveTransfer(req, res));

        this._app.post('/payments/zus-transfer/sign',
            (req: Request, res: Response) => this._paymentsController.signTransfer(req, res));
    }
}
